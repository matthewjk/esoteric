#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <map>

class Tape{
    private:
        std::vector<int> tape;
        int pointer;

    public:
        Tape(){
            pointer = 0;
            tape.push_back(0);
        }
        ~Tape(){}

        int get(){return tape[pointer];}

        void right(){
            pointer++;
            if(tape.size()<=pointer){
                tape.push_back(0);
            }
        }

        void left(){
            if(pointer>0){
                pointer--;
            }
        }

        void increment(){
            tape[pointer]++;
        }

        void decrement(){
            tape[pointer]--;
        }

        void output(){
            std::cout<<(char)tape[pointer];
        }

        void input(){
            char c;
            std::cout << "input (char): ";
            std::cin >> c;
            tape[pointer] = (int)c;
        }
};

class Program{
    private:
        std::string code;
        std::map<int,int> bracketmap;

    public:
        Program(std::string text){
            std::vector<int> pending; 
            std::string commands = "><+-.,[]";

            for(int i=0;i<text.length();i++){
                if(commands.find(text[i])!=std::string::npos){
                    if(text[i]=='['){
                        pending.push_back(i);
                    }
                    else if(text[i]==']'){
                        bracketmap.insert(std::make_pair(pending[pending.size()-1],i));
                        bracketmap.insert(std::make_pair(i,pending[pending.size()-1]));
                        pending.pop_back();
                    }
                    code.push_back(text[i]);
                }
            }
        }
        ~Program(){}

        void printbracketmap(){
            std::map<int, int>::iterator it = bracketmap.begin();
            while(it != bracketmap.end()){
            std::cout<<it->first<<" :: "<<it->second<<std::endl;
            it++;
            }
        }

        void execute(){
            Tape tape;
            for(int counter = 0;counter<code.length();counter++){
                switch(code[counter]){
                    case '>':
                        tape.right();
                        break;
                    case '<':
                        tape.left();
                        break;
                    case '+':
                        tape.increment();
                        break;
                    case '-':
                        tape.decrement();
                        break;
                    case '.':
                        tape.output();
                        break;
                    case ',':
                        tape.input();
                        break;
                    case '[':
                        if(tape.get() == 0){
                            counter = bracketmap[counter];
                        }
                        break;
                    case ']':
                        if(tape.get()!=0){
                            counter = bracketmap[counter];
                        }
                        break;
                    default:
                        break;
                }
            }
        }
};

std::string readfile(std::string filename){
    std::ifstream filestream(filename);
    if(filestream.is_open()){
        filestream.seekg(0,filestream.end);
        int filelength = filestream.tellg();
        filestream.seekg(0,filestream.beg);

        std::string text(filelength,' ');
        filestream.read(&text[0],filelength);
        filestream.close();
        return text;
    }
    else{
        std::cout<<"Failed to open file"<<std::endl;
        return "";
    }
}

// Specify location of bf code as argument
int main(int argc, char** argv){
    if(argc!=2){
        std::cout<<"Incorrect number of arguments passed"<<std::endl;
    }
    else{
        std::cout<<argv[1]<<std::endl;
        std::string filename = argv[1];
        std::string text = readfile(filename);
        if(text!=""){
            std::cout<<text<<std::endl;
        }
        Program p(text);
        p.execute();
    }
    return 0;
}
